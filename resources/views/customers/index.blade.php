@extends('layouts.app')
@section('content')

<body style="background-color:lightgray;">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<h1>Customers List</h1>
<br>
<a href = "{{route('customers.create')}}">Create new customers </a>
<ul>
<br>

@foreach($customers as $customer)
@if($customer->user->id==$userid)
<li style = "font-weight:bold">
@else
<li>
@endif
    @if($customer->status)
<span style="color:green">{{$customer->name}}, {{$customer->email}}, {{$customer->phone}}, Created by: {{$customer->user->name}}</span>
    @else
<span>{{$customer->name}}, {{$customer->email}}, {{$customer->phone}}, Created by: {{$customer->user->name}}</span>
@endif
    <a href = "{{route('customers.edit',$customer->id)}}">Edit Details</a>
    @can('manager')
    <a style="color:red" href="{{route('customers.delete' , $customer->id)}}"> Delete</a> 
    @if(!($customer->status))
    <a href = "{{route('customers.deal',$customer->id)}}">Deal Closed</a>
@endif
    @endcan
    @cannot('manager')
    Delete         
    @endcannot
</li>
@endforeach
</ul>
@endsection
</body>