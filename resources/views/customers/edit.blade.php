@extends('layouts.app')
@section('content')


<h1>Edit Customer</h1>
<br>
<form method = 'POST' action = "{{action('CustomerController@update' , $customer->id)}}">
@method('PATCH')
@csrf
<div class = "form-group">
<label for = "title">Customer Name:</label>
<input type = "text" class= "form-control" name = "name" value = "{{$customer->name}}">
</div>
<br>
<div class = "form-group">
<label for = "title">Customer Email:</label>
<input type = "text" class= "form-control" name = "email" value = "{{$customer->email}}">
</div>
<br>
<div class = "form-group">
<label for = "title">Customer Phone Number:</label>
<input type = "text" class= "form-control" name = "phone" value = "{{$customer->phone}}">
</div>
<br>
<br>
<br>
<br>
<br>
<div class = "form-group">
<input type = "submit" class= "form-control" name = "submit" value = "Save">
</div>
</form>


@endsection

